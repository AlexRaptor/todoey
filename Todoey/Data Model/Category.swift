//
//  Category.swift
//  Todoey
//
//  Created by Александр Селиванов on 12.07.2018.
//  Copyright © 2018 Alexander Selivanov. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    
    @objc dynamic var name: String = ""
    @objc dynamic var color: String = ""
    let items = List<Item>()
    
}
